from ftplib import FTP
from server_config import get_server_ip

SERVER_IP = get_server_ip()
SERVER_PORT = 2121


class FtpClient:
    def __init__(self):
        self.ftp = FTP('')
        self.ftp.connect(SERVER_IP, SERVER_PORT)
        self.ftp.login()
        self.ftp.cwd('.')

    def list_files(self):
        # List the content of FTP Server directory
        self.ftp.retrlines('LIST')

        # Return the content
        files = self.ftp.mlsd(facts=[])
        return files

    def __download_file(self, file_name):
        print("Downloading {}...".format(file_name))
        local_file = open(file_name, 'wb')
        self.ftp.retrbinary('RETR ' + file_name, local_file.write)
        local_file.close()
        print("Downloading {} completed".format(file_name))

    """
    def upload_file(self, file_name):
        self.ftp.storbinary('STOR '+file_name, open(file_name, 'rb'))
    """

    def download_patch_files(self, client_version_str, server_version_str):
        # Convert the server/client versions to integer
        client_version = int(client_version_str)
        server_version = int(server_version_str)

        # Download files with .patch extension and newer than the client version
        for f in self.list_files():
            if f[0].endswith('.patch') and int(f[0].split('.')[0]) in range(client_version + 1, server_version + 1):
                self.__download_file(f[0])

        # Close FTP Connection
        self.ftp.quit()
