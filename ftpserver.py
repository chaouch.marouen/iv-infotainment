#!/usr/bin/env python3

from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
from pyftpdlib.authorizers import DummyAuthorizer
import os
from server_config import get_server_ip

SERVER_IP = get_server_ip()
SERVER_PORT = 2121
SHARED_DIR = './updates'


class MyHandler(FTPHandler):

    def on_connect(self):
        print("%s:%s connected" % (self.remote_ip, self.remote_port))

    def on_disconnect(self):
        # do something when client disconnects
        pass

    def on_login(self, username):
        # do something when user login
        pass

    def on_logout(self, username):
        # do something when user logs out
        pass

    def on_file_sent(self, file):
        # do something when a file has been sent
        pass

    def on_file_received(self, file):
        # do something when a file has been received
        pass

    def on_incomplete_file_sent(self, file):
        # do something when a file is partially sent
        pass

    def on_incomplete_file_received(self, file):
        # remove partially uploaded files
        os.remove(file)


def main():
    authorizer = DummyAuthorizer()
    authorizer.add_user('admin', 'password', homedir=SHARED_DIR, perm='elradfmwMT')
    authorizer.add_anonymous(homedir=SHARED_DIR)

    handler = MyHandler
    handler.authorizer = authorizer

    try:
        server = FTPServer((SERVER_IP, SERVER_PORT), handler)
        server.serve_forever()
    except OSError:
        print("Error starting FTP Server! Please server configuration.")


if __name__ == "__main__":
    main()
