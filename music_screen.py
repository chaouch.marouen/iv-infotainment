from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty
import os
from subprocess import call
from kivy.lang.builder import Builder
import vlc
from test_android import get_devices

Builder.load_file("res/layouts/music_screen_layout.kv")

directory = os.path.expanduser('~/mediaserver')


class MusicPlayerScreen(Screen):
    ipaddress_text_input = ObjectProperty()
    status_label = ObjectProperty()
    myfileChooser = ObjectProperty()

    port = "3721"

    instance = vlc.Instance()
    player = instance.media_player_new()

    def connect(self):
        print('Connecting to ftpserver...')

        # Prepare mount directory for the FTP Server
        if not os.path.exists(directory):
            print('Creating ~/mediaserver directory...')
            os.makedirs(directory)
            print('~/mediaserver created...')
        else:
            print('~/mediaserver directory already exists...')

        # Unmount mediaserver
        os.system("fusermount -u ~/mediaserver")

        # System command to mount the FTP Server on ~/mediaserver/
        if call(["curlftpfs", "ftp://" + self.ipaddress_text_input.text + ":" + self.port + "/", directory]) == 0:
            msg_label = 'Connected'

            self.ids['connect_btn'].disabled = True
            self.ids['disconnect_btn'].disabled = False

            # Enable the Filechooser for selection and update its content
            self.ids['filechooser'].path = '~/mediaserver'
            self.ids['filechooser'].disabled = False
            self.ids['filechooser']._update_files()

        else:
            msg_label = 'Connexion failed'

        self.status_label.text = msg_label

    def disconnect(self):

        print('Unmounting ftpserver...')

        # Stop player if playing before disconnecting
        self.player.stop()

        # System command to unmount the FTP Server
        if call(["fusermount", "-u", directory]) == 0:
            print('Unmount Successful')

            self.ipaddress_text_input.text = ""
            self.status_label.text = 'Disconnected'

            self.ids['connect_btn'].disabled = False
            self.ids['disconnect_btn'].disabled = True

            self.ids['filechooser']._update_files()
            self.ids['filechooser'].disabled = True

    def play_selection(self):

        if self.ids['filechooser'].selection:
            path = self.ids['filechooser'].selection[0]

            # get only the file name from full path
            song_name = path.rpartition('/')[-1]

            # Display the song name
            self.ids['song_label'].text = song_name

            # Play the media file
            self.media = self.instance.media_new(path)
            self.player.set_media(self.media)

            self.player.play()

    def stop(self):
        self.player.stop()

        # Reset the Playing song label
        self.ids['song_label'].text = ""

    def pause(self):
        self.player.pause()

    def on_connect(self):
        devices = get_devices()
        print(devices)

        if not devices:
            self.status_label.text = "No Device found"
        else:
            self.ipaddress_text_input.text = devices[0]
            self.status_label.text = "Connected"
            self.connect()
