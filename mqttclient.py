#! /usr/bin/env python3

"""
MQTT Client script that checks for updates in broker, run periodically in background with crontab
"""
import paho.mqtt.client as mqtt
import version
import time
import sys
from uuid import getnode as get_mac
import os
from server_config import get_server_ip

os.chdir(os.path.join(os.getenv("HOME"), 'iv-infotainment'))

# Get MAC address of the machine
MAC = hex(get_mac())

BROKER_ADDRESS = get_server_ip()
VERSION_TOPIC = "version"
SERVER_VERSION = ""


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        print('Connected to ', BROKER_ADDRESS)
    else:
        print("Bad connection, returned code=", rc)
        mqtt.Client.bad_connection_flag = True


def on_disconnect(client, userdata, flags, rc=0):
    print("Disconnected result code " + str(rc) + "")
    client.connected_flag = True


def on_log(client, userdata, level, buf):
    print("log:", buf)


def on_message(client, userdata, msg):
    global SERVER_VERSION
    SERVER_VERSION = msg.payload.decode()


def check_update():
    global SERVER_VERSION
    mqtt.Client.connected_flag = False
    mqtt.Client.bad_connection_flag = False

    # Make client ID unique with MAC address
    mqttclient = mqtt.Client(MAC)
    mqttclient.on_connect = on_connect
    mqttclient.on_disconnect = on_disconnect
    mqttclient.on_log = on_log
    mqttclient.on_message = on_message

    print("Connecting to Broker ", BROKER_ADDRESS, "...")
    try:
        mqttclient.connect(BROKER_ADDRESS)
    except:
        print("Connection failed")
        sys.exit(1)

    mqttclient.loop_start()

    # Wait for connection establishment
    while not mqttclient.connected_flag and not mqttclient.bad_connection_flag:
        print("In wait loop")
        time.sleep(1)
        if mqttclient.bad_connection_flag:
            mqttclient.loop_stop()
            sys.exit(1)

    print("In main loop")
    print("Subscribing to topic: ", VERSION_TOPIC)
    mqttclient.subscribe(VERSION_TOPIC, 1)

    time.sleep(4)

    mqttclient.loop_stop()
    mqttclient.disconnect()

    print("SERVER Message: " + SERVER_VERSION)

    print("Checking if it is newer...")
    if SERVER_VERSION > version.get_current_version() and SERVER_VERSION != "":
        print("Writing the new version to 'version_latest.txt'")
        with open('version_latest.txt', 'w') as latest:
            latest.write(SERVER_VERSION)


check_update()
