from kivy.uix.screenmanager import Screen
from kivy.lang.builder import Builder
from ftpclient import FtpClient
import version
from kivy.properties import ObjectProperty
import os
import shutil
import subprocess
from server_config import get_server_ip
from kivy.app import App

Builder.load_file("res/layouts/update_screen_layout.kv")

BACKUP_DIR = '../' + os.path.basename(os.getcwd()) + '.old'
SERVER_IP = get_server_ip()


class UpdateScreen(Screen):
    new_version_label = ObjectProperty()
    update_button = ObjectProperty()

    def __init__(self, **kwargs):
        super(UpdateScreen, self).__init__(**kwargs)
        self.update_new_version_label()

    def update_new_version_label(self):
        self.new_version_label.text = "New version is available: v." + version.get_latest_version()

    def download_update(self):
        print("Downloading updates...")
        self.new_version_label.text += "\nDownloading patch files..."

        client = FtpClient()
        client.download_patch_files(version.get_current_version(), version.get_latest_version())

        print("Download completed")
        self.new_version_label.text += "\nDownload completed"

    def apply_updates(self):
        # Get patch files list
        patch_files = [f for f in os.listdir('.') if f.endswith('.patch')]

        # Make sure the patch files list is sorted
        patch_files = sorted(patch_files)
        print("patch files: ", patch_files)

        if patch_files:

            try:
                for f in patch_files:
                    print("Verifying patch: " + f + '...')
                    self.new_version_label.text += "\nVerifying patch: " + f + '...'

                    subprocess.check_output("git apply --check " + f, shell=True)

                    print("Patch verified")
                    self.new_version_label.text += "\nPatch verified"

                    print("Applying patch ", f)
                    self.new_version_label.text += "\nApplying patch: " + f + '...'

                    subprocess.call("git apply " + f, shell=True)

                    print("Applied " + f + " Successful")
                    self.new_version_label.text += "\nPatch Applied"

                    print("Removing " + f)
                    self.new_version_label.text += "\nRemoving patch file: " + f + '...'

                    os.remove(f)

                    print(f + " removed")
                    self.new_version_label.text += "\nPatch file removed"

                print('Update successful')
                self.new_version_label.text += "\nUpdate Completed\nRestart to see changes"
                self.ids['update'].disabled = True
                self.ids['restart'].disabled = False

                print("Backup new version...")
                self.new_version_label.text += "\nBacking up..."

                # Empty the old BACKUP
                if os.path.exists(BACKUP_DIR):
                    shutil.rmtree(BACKUP_DIR)
                shutil.copytree(os.getcwd(), BACKUP_DIR)

                print("Backup completed")
                self.new_version_label.text += "\nBackup completed"

            except subprocess.CalledProcessError as err:

                print(err)
                self.new_version_label.text += "\nError occured..."

                # Restore
                print("Restoring...")
                self.new_version_label.text += "\nRestoring..."

                shutil.rmtree(os.getcwd())
                os.rename(BACKUP_DIR, '../iv-infotainment')

                print("Restore completed")
                self.new_version_label.text = "\nError occurred during update. Restore completed"

    def update(self):
        self.download_update()
        self.apply_updates()

    def exit_app(self):
        App.get_running_app().stop()
