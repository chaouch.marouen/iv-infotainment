#!/usr/bin/env python3

import kivy

kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
# from kivy.core.window import Window
import os
import shutil
from kivy.config import Config
from main_screen import MainScreen
from music_screen import MusicPlayerScreen
from radio_screen import OnlineRadioScreen
from weather_screen import WeatherScreen
from maps_screen import MapsScreen
from update_screen import UpdateScreen

os.chdir(os.path.join(os.getenv("HOME"), 'iv-infotainment'))

"""
Fixed resolution on 800x480 and/or Fullscreen (Raspi official Touchscreen)
"""
# Window.fullscreen = 'auto'
Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '480')
Config.set('kivy', 'keyboard_mode', 'systemandmulti')


class InfotainmentApp(App):

    def build(self):

        # Prepare latest version file
        if not os.path.exists('version_latest.txt'):
            shutil.copy2('version.txt', 'version_latest.txt')

        screen_manager = ScreenManager()
        screen_manager.add_widget(MainScreen(name='MainScreen'))
        screen_manager.add_widget(MusicPlayerScreen(name='MusicPlayerScreen'))
        screen_manager.add_widget(OnlineRadioScreen(name='OnlineRadioScreen'))
        screen_manager.add_widget(WeatherScreen(name='WeatherScreen'))
        screen_manager.add_widget(MapsScreen(name='MapsScreen'))
        screen_manager.add_widget(UpdateScreen(name='UpdateScreen'))

        return screen_manager


if __name__ == '__main__':
    InfotainmentApp().run()

# Testing
