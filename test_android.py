import subprocess
from server_config import get_server_ip
from test_ftp import test_ftp
import nmap

FTP_PORT = '3721'


def get_head_sep(ip):
    head, sep, tail = ip.rpartition('.')
    return head, sep


def get_subnet(ip):
    head, sep = get_head_sep(ip)
    return head + sep + '0/24'


def get_my_ip(ip):
    head, sep = get_head_sep(ip)
    output = output = subprocess.check_output("ifconfig", shell=True)
    my_ip = str(output).split(head + sep)[1].split(' ')[0]
    return head + sep + my_ip


def get_devices():
    server_ip = get_server_ip()

    nm = nmap.PortScanner()
    nm.scan(hosts=get_subnet(server_ip), ports=FTP_PORT, arguments='-n')

    host_list = []
    for host in nm.all_hosts():
        if host not in (server_ip, get_my_ip(server_ip)):
            if test_ftp(host, FTP_PORT):
                host_list.append(host)

    return host_list


# print(get_devices())
