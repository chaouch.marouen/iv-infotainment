import requests
import json


def get_location():
    try:
        response_data = requests.get('https://www.iplocation.net/go/ipinfo').text
        response_json_data = json.loads(response_data)
        location = response_json_data["loc"].split(",")

        # lat = location[0]
        # lon = location[1]
        # print("Latitude: {}".format(lat))
        # print("Longitude: {}".format(lon))

        return location
    except:
        print("Exception happened while loading data")
        return None
