#!/usr/bin/env python3

from subprocess import call
from os import system
from datetime import datetime
import paho.mqtt.client as mqtt
import version
import time
import sys
from server_config import  get_server_ip

PATCH_DIR = './updates/'
PATCH_FILE = ""

BROKER_ADDRESS = get_server_ip()
VERSION_TOPIC = "version"


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        print('Connected to ', BROKER_ADDRESS)
    else:
        print("Bad connection, returned code=", rc)
        mqtt.Client.bad_connection_flag = True


def on_disconnect(client, userdata, flags, rc=0):
    print("Disconnected result code " + str(rc) + "")
    client.connected_flag = True


def on_log(client, userdata, level, buf):
    print("log:", buf)


# Check if there is a argument as a message for the commit
if len(sys.argv) != 2:
    print('Command require 1 argument: commit message "".')
else:
    # Get the current date and time
    now = datetime.now().strftime("%Y%m%d%H%M%S")
    print(now)

    # Save version as current date and time
    with open('version.txt', 'w') as f:
        f.write(str(now))

    with open('version_latest.txt', 'w') as f:
        f.write(str(now))

    SERVER_VERSION = version.get_current_version()

    # Add index for files
    call(['git', 'add', '.'])

    # Commit changes
    call(['git', 'commit', '-am', sys.argv[1]])

    # generate the patch file name with the current date and time
    PATCH_FILE = str(now) + '.patch'

    # Generate the patch file
    system('git format-patch -1 --stdout > ' + PATCH_DIR + PATCH_FILE)

    # Notify about the new version

    # Log the patch version
    with open('updates.log', 'a', encoding='utf-8') as f:
        f.write(PATCH_FILE)
        f.write('\n')

    mqtt.Client.connected_flag = False
    mqtt.Client.bad_connection_flag = False
    mqttserver = mqtt.Client("SERVER")
    mqttserver.on_connect = on_connect
    mqttserver.on_disconnect = on_disconnect
    mqttserver.on_log = on_log

    print("Connecting to Broker ", BROKER_ADDRESS, "...")
    try:
        mqttserver.connect(BROKER_ADDRESS)
    except:
        print("Connection failed")
        sys.exit(1)

    mqttserver.loop_start()

    while not mqttserver.connected_flag and not mqttserver.bad_connection_flag:
        print("In wait loop")
        time.sleep(1)
        if mqttserver.bad_connection_flag:
            mqttserver.loop_stop()
            sys.exit(1)

    print("In main loop")
    print("Publishing to topic: ", VERSION_TOPIC, SERVER_VERSION)
    mqttserver.publish(VERSION_TOPIC, SERVER_VERSION, 1, True)

    time.sleep(4)

    mqttserver.loop_stop()
    mqttserver.disconnect()
