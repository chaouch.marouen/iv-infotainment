from kivy.uix.screenmanager import Screen
from kivy.lang.builder import Builder
import vlc
import time

Builder.load_file("res/layouts/radio_screen_layout.kv")


class OnlineRadioScreen(Screen):
    instance = vlc.Instance()
    media = ""
    player = instance.media_player_new()

    def play_radio(self, button_instance, radio_url):
        """
        Play online radio station.
        """
        self.media = self.instance.media_new(radio_url)
        self.player.set_media(self.media)
        self.player.play()

        # Sleep for VLC to complete retries.
        time.sleep(2)

        # Get current state.
        state = str(self.player.get_state())

        if state == "State.Ended" or state == "State.Error":
            print('Stream is dead. Current state = {}'.format(state))
            self.ids['radio_name_label'].text = "Connection Error"
            self.player.stop()
        else:
            print('Stream is working. Current state = {}'.format(state))

            # Display the playing radio station
            self.ids['radio_name_label'].text = button_instance.text

    def stop_radio(self):
        self.player.stop()

        # Reset the playing now radio label
        self.ids['radio_name_label'].text = ""
