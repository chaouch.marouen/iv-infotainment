import filecmp
from test_ftp import test_ftp
import version
import time
from kivy.properties import ObjectProperty
from kivy.clock import Clock
from functools import partial
from server_config import get_server_ip
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from kivy.lang import Builder
from kivy.app import App

Builder.load_file("res/layouts/main_screen_layout.kv")


class MainScreen(Screen):
    clock_label = ObjectProperty()
    version_label = ObjectProperty()

    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)

        # set main menu version label
        self.set_currrent_ver()

        # Update clock every second
        Clock.schedule_interval(partial(self.set_clock, 'my value', 'my key'), 1)

        # Check update on startup
        Clock.schedule_once(self.check_update, 1)

    def set_clock(self, value, key, *largs):
        self.clock_label.text = time.asctime()

    def exit_app(self):
        App.get_running_app().stop()

    def set_currrent_ver(self):
        self.version_label.text = 'v.' + version.get_current_version()

    def check_update(self, dt):
        # Test FTP Connection and if it has the latest version
        if test_ftp(get_server_ip(), '2121') and not filecmp.cmp('version.txt', 'version_latest.txt',
                                                                shallow=False):

            # Enable the Firmware Screen button
            self.ids['firmware'].disabled = False

            # show popup of new available update
            popup_label = Label(text="New version is available: v." + version.get_latest_version())
            ok_btn = Button(text='OK')
            layout = BoxLayout(orientation='vertical')
            layout.add_widget(popup_label)
            layout.add_widget(ok_btn)
            popup = Popup(title='UPDATE',
                          content=layout,
                          size_hint=(None, None), size=(400, 250))
            ok_btn.bind(on_release=popup.dismiss)
            popup.open()

# testing
