from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty
from kivy.lang import Builder
import requests
from geolocation import get_location
from kivy.clock import Clock
from functools import partial
from collections import defaultdict

Builder.load_file("res/layouts/weather_screen_layout.kv")


class WeatherScreen(Screen):
    weather_status_label = ObjectProperty()
    weather_data_text_input = ObjectProperty()

    weatherapi_url_template = 'http://api.openweathermap.org/data/2.5/weather?appid={}&units=metric&lat={}&lon={}'
    appid = "72a53fc44ee5c8f1e4fa6c3bb49eb904"

    def __init__(self, **kwargs):
        super(WeatherScreen, self).__init__(**kwargs)
        self.get_weather()
        Clock.schedule_interval(partial(self.get_weather, 'my value', 'my key'), 60)

    def get_weather(self, value=0, key=0, *largs):
        location = get_location()

        # Testing location variable is empty
        if location is None:
            self.weather_status_label.text = "No internet connection"
        else:

            # Prepare the query url
            url = self.weatherapi_url_template.format(self.appid, location[0], location[1])

            try:
                # Get request of the json data
                json_data = defaultdict(int, requests.get(url).json())

                # Display the json
                self.weather_status_label.text = "Connected"
                self.weather_ouput(json_data)

            except:
                print("Exception happened while loading data")

    def weather_ouput(self, json):
        self.weather_data_text_input.text = "Location:\t" + json['name'] + ", " + json['sys']['country']
        self.weather_data_text_input.text += "\nCoordinates:\t" + str(json['coord']['lon']) + "°E, " + str(
            json['coord']['lat']) + "°N"
        self.weather_data_text_input.text += "\n\nWeather:\t" + str(json['weather'][0]['main'])
        self.weather_data_text_input.text += "\nDescription:\t" + str(json['weather'][0]['description'])
        self.weather_data_text_input.text += "\n\nTemp:\t" + str(json['main']['temp']) + "°C"
        self.weather_data_text_input.text += "\nMin:\t" + str(json['main']['temp_min']) + "°C"
        self.weather_data_text_input.text += "\nMax:\t" + str(json['main']['temp_max']) + "°C"
        self.weather_data_text_input.text += "\nPressure:\t" + str(json['main']['pressure']) + " hPa"
        self.weather_data_text_input.text += "\nVisibility:\t" + str(json['visibility']) + " m"
        self.weather_data_text_input.text += "\nHumidity:\t" + str(json['main']['humidity']) + "%"
