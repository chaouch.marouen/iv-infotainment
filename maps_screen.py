from kivy.uix.screenmanager import Screen
from kivy.lang.builder import Builder

Builder.load_file("res/layouts/maps_screen_layout.kv")


class MapsScreen(Screen):
    pass
