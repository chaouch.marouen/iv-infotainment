import os

os.chdir(os.path.join(os.getenv("HOME"), 'iv-infotainment'))


def get_server_ip():
    with open('server_ip.txt') as f:
        return f.read()
