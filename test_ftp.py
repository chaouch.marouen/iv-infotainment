import subprocess


def test_ftp(ip, port):
    try:
        subprocess.check_output("nc -zvv -w 5 " + ip + " " + port + " 2>&1 | grep succeeded", shell=True)
        return True
    except:
        return False
