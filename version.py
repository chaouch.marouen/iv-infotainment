import os

os.chdir(os.path.join(os.getenv("HOME"), 'iv-infotainment'))


def get_current_version():
    with open('version.txt') as f:
        return f.read()


def get_latest_version():
    with open('version_latest.txt') as f:
        return f.read()
