# In-Vehicule Infotainment
Based on Raspberry Pi 3 and official 7" touchscreen and Raspbian OS,   Developed with Python 3 and Kivy GUI Framework.

## Features
- Scan and auto-connect to Android phone via Wi-Fi AP to play music content  
(use an Android FTP server like ES File Explorer; https://play.google.com/store/apps/details?id=com.estrongs.android.pop)
- Online Radio Stations
- IP Geolocation
- Weather
- Maps
* Support MQTT update notification
* Support Firmware Over-the-Air (FOTA) updates

<img src="screenshots/main.png" />

## Getting Started
### Prerequisites
#### Server side: (Local Server):  
```
sudo apt update && sudo apt full-upgrade
```
```
sudo apt install python3 python3-pip git mosquitto mosquitto-clients  
```
```
sudo pip3 install paho-mqtt pyftpdlib
```

#### Client(s) side (Raspberry Pi 3):  

```
sudo apt update && sudo apt full-upgrade
```
```
sudo apt install python3 python3-pip git curlftpfs vlc nmap python3-nmap  
```
install Kivy Framework: https://kivy.org/doc/stable/installation/installation-rpi.html  
```
sudo pip3 install python-vlc paho-mqtt kivy-garden  
```
```
sudo garden install mapview  
```
Setup a Wi-Fi Access Point: https://github.com/Phoenix1747/RouteryPi

### Deployment
1) Install the project in Home directory of both the server and client(s)
2) Set your local server IP address in "server_ip.txt"

#### Server side:
Start Mosquitto Broker in verbose mode for logging purposes
```
mosquitto -v
```
Start the FTP Server
```
python3 ftpserver.py
```

#### Client(s) side:
##### Check update background process:
Setup a crontab job for MQTT Client (check update every minute):  
```
crontab -e
```
Add line:
```
* * * * * python3 /home/pi/iv-infotainment/mqttclient.py
```
##### Auto-startup / app restart after update feature:
Setup the systemd service
```
sudo cp /home/pi/iv-infotainment/systemd_service/infotainment.service /lib/systemd/system/
```
Enable the service and reboot
```
sudo systemctl daemon-reload
sudo systemctl enable infotainment.service
sudo reboot
```
To check service status
```
sudo systemctl status infotainment.service
```

### Testing FOTA:
#### Server side:
1) To generate a patch for new update and notify clients, just run
```
python3 create_patch_notifiy.py
```
#### Client(s) side:  
0) If an new update is detected, a popup notification will show up at the next app startup
1) Go to firmware update screen to update manually
